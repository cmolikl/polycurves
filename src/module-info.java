module PolyCurves {
    requires java.desktop;

    exports polynomialcurves;
    exports ui.control;
    exports ui.parameter;
    exports javax.vecmath;
}