/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.Dimension;
import java.io.*;
import java.net.URL;
import javax.swing.*;

/**
 *
 * @author cmolikl
 */
public class Main {
    public static void main(String... args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("The atempt to set system Look&Feel failed. Continuing with default.");
        }

        String aboutText = "Error while loading the text.";
        try {
            InputStream aboutStream = ClassLoader.getSystemResourceAsStream("about.html");
            BufferedReader reader = new BufferedReader(new InputStreamReader(aboutStream));
            String line = reader.readLine();
            if(line != null) {
                aboutText = "";
            }
            while(line != null) {
                aboutText += line + "\n";
                line = reader.readLine();
            }
        }
        catch(FileNotFoundException e) {
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        JEditorPane editorPane = new JEditorPane();
        editorPane.setContentType("text/html");
        editorPane.setText(aboutText);
        editorPane.setEditable(false);

        JScrollPane about = new JScrollPane(editorPane);
        
        // Put the canvas to JFrame
        JFrame frame = new JFrame("PolyCurves");
        frame.setSize(1000, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        BezierExample example1 = new BezierExample();
        example1.setPreferredSize(new Dimension(800, 800));
        BezierPiecewiseExample example2 = new BezierPiecewiseExample();
        example2.setPreferredSize(new Dimension(800, 800));
        BSplineExample example4 = new BSplineExample();
        example4.setPreferredSize(new Dimension(800, 800));
        
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("About", about);
        tabbedPane.addTab("Bezier segment", example1);
        tabbedPane.addTab("Bezier piecewise curve", example2);
        tabbedPane.addTab("B-spline curve", example4);
        
        frame.getContentPane().add(tabbedPane);
        frame.setVisible(true);
    }
}
