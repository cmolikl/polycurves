/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.BorderLayout;
import java.awt.Dimension;

import ui.control.*;

import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point3f;

import ui.parameter.FloatParameter;
import ui.parameter.IntParameter;

/**
 *
 * @author cmolikl
 */
public class BSplineExample extends JPanel{
    public BSplineExample() {
        
        ControlPoint p0 = new ControlPoint(new Point3f(100f, 500f, 1f));
        ControlPoint p1 = new ControlPoint(new Point3f(100f, 300f, 1f)); 
        ControlPoint p2 = new ControlPoint(new Point3f(400f, 100f, 1f)); 
        ControlPoint p3 = new ControlPoint(new Point3f(700f, 300f, 1f));
        ControlPoint p4 = new ControlPoint(new Point3f(700f, 500f, 1f));
        ControlPoint p5 = new ControlPoint(new Point3f(500f, 500f, 1f));
        ControlPoint p6 = new ControlPoint(new Point3f(400f, 300f, 1f));
        
        ControlPoint[] cp = new ControlPoint[] {p0, p1, p2, p3, p4, p5, p6};
        
        IntParameter order = new IntParameter("order", 4);
        
        //float[] knots = new float[] {1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f, 11f};
        
        BSplineCurve curve = new BSplineCurve(cp, order.getValue());
        
        
        // Map data to graphical elements
        ArrayList<Curve> scene = new ArrayList<>();
        scene.add(curve);
        
        // Create instance of Canvas and provide the graphical elements
        Dimension size = new Dimension(1000, 500);
        Canvas canvas = new Canvas(scene);
        canvas.setMaximumSize(size);
        canvas.setMinimumSize(size);
        canvas.setPreferredSize(size);
        
        size = new Dimension(1000, 200);
        BSplineParametrization parametrization = new BSplineParametrization(curve, canvas);
        parametrization.setMaximumSize(size);
        parametrization.setMinimumSize(size);
        parametrization.setPreferredSize(size); 
        
        order.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                IntParameter p = (IntParameter) e.getSource();
                curve.setOrder(p.getValue());
                canvas.repaint();
                parametrization.repaint();
            }
        });
        
        ChangeListener repaintListener = new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                canvas.repaint();
            }
        };
        

        IntSlider orderSlider = new IntSlider(order, JSlider.HORIZONTAL, 2, 4);
        
        FloatParameter t = new FloatParameter("t", 0.5f);
        t.addChangeListener(repaintListener);
        curve.deBoorT = t;
        FloatSlider tSlider = new FloatSlider(t, false, JSlider.HORIZONTAL, 0f, 1f);
        
        curve.paintDeBoor.addChangeListener(repaintListener);
        BooleanCheckBox paintDeBoor = new BooleanCheckBox("Show deBoor alg.", curve.paintDeBoor);

        curve.displaySegments.addChangeListener(repaintListener);
        BooleanCheckBox displaySegmentsBox = new BooleanCheckBox("Display segments", curve.displaySegments);
        
        
        size = new Dimension(200, 800);
        JPanel panel = new JPanel();
        panel.setMaximumSize(size);
        panel.setMinimumSize(size);
        panel.setPreferredSize(size);
        //panel.add(new ParameterLabel(threshold));
        //panel.add(thresholdSlider);
        panel.add(new ParameterLabel(t));
        panel.add(tSlider);
        panel.add(new ParameterLabel(order));
        panel.add(orderSlider);
        panel.add(paintDeBoor);
        panel.add(displaySegmentsBox);
        
        // Put the canvas to panel
        //this.setLayout(new BorderLayout());
        //this.add(canvas, BorderLayout.CENTER);
        //this.add(parametrization, BorderLayout.SOUTH);
        //this.add(panel, BorderLayout.EAST);
        
        SpringLayout layout1 = new SpringLayout();
        this.setLayout(layout1);
        this.add(canvas);
        this.add(parametrization);
        this.add(panel);
        layout1.putConstraint(SpringLayout.WEST, canvas, 0, SpringLayout.WEST, this);
        layout1.putConstraint(SpringLayout.NORTH, canvas, 0, SpringLayout.NORTH, this);
        
        layout1.putConstraint(SpringLayout.EAST, panel, 0, SpringLayout.EAST, this);
        layout1.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.NORTH, this);
        
        layout1.putConstraint(SpringLayout.EAST, canvas, 0, SpringLayout.WEST, panel);
        
        layout1.putConstraint(SpringLayout.WEST, parametrization, 0, SpringLayout.WEST, this);
        layout1.putConstraint(SpringLayout.SOUTH, parametrization, 0, SpringLayout.SOUTH, this);
        
        layout1.putConstraint(SpringLayout.EAST, parametrization, 0, SpringLayout.WEST, panel);
        
        layout1.putConstraint(SpringLayout.SOUTH, canvas, 0, SpringLayout.NORTH, parametrization);
        layout1.putConstraint(SpringLayout.SOUTH, panel, 0, SpringLayout.NORTH, parametrization);
    }
}
