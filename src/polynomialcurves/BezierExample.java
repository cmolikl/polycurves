/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.Dimension;

import ui.control.*;

import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point3f;

import ui.parameter.FloatParameter;
import ui.parameter.IntParameter;

/**
 *
 * @author cmolikl
 */
public class BezierExample extends JPanel {
    
    Canvas canvas;

    public BezierExample() {
        
        IntParameter degree = new IntParameter("degree", 3);

        Point3f p0 = new Point3f(100f, 500f, 1f);
        Point3f p1 = new Point3f(200f, 100f, 1f);
        Point3f p2 = new Point3f(300f, 100f, 1f);
        Point3f p3 = new Point3f(400f, 500f, 1f);
        Point3f p4 = new Point3f(600f, 500f, 1f);
        Point3f p5 = new Point3f(700f, 500f, 1f);
        Point3f p6 = new Point3f(800f, 500f, 1f);
        
        BezierCurve s1 = new BezierCurve(new ControlPoint(p0), new ControlPoint(p1));
        BezierCurve s2 = new BezierCurve(new ControlPoint(p0), new ControlPoint(p1), new ControlPoint(p2));
        BezierCurve s3 = new BezierCurve(new ControlPoint(p0), new ControlPoint(p1), new ControlPoint(p2), new ControlPoint(p3));
        BezierCurve s4 = new BezierCurve(new ControlPoint(p0), new ControlPoint(p1), new ControlPoint(p2), new ControlPoint(p3), new ControlPoint(p4));
        BezierCurve s5 = new BezierCurve(new ControlPoint(p0), new ControlPoint(p1), new ControlPoint(p2), new ControlPoint(p3), new ControlPoint(p4), new ControlPoint(p5));
        BezierCurve s6 = new BezierCurve(new ControlPoint(p0), new ControlPoint(p1), new ControlPoint(p2), new ControlPoint(p3), new ControlPoint(p4), new ControlPoint(p5), new ControlPoint(p6));
        
        BezierCurve[] segment = {s1, s2, s3, s4, s5, s6};

        // Map data to graphical elements
        ArrayList<Curve> scene = new ArrayList<>();
        scene.add(segment[degree.getValue()-1]);

        // Create instance of Canvas and provide the graphical elements
        Dimension cSize = new Dimension(1000, 800);
        canvas = new Canvas(scene);
        canvas.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
        canvas.setMinimumSize(new Dimension(200, 200));
        canvas.setPreferredSize(new Dimension(800, 800));
        
        ChangeListener repaintListener = new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                canvas.repaint();
            }
        };
        
        degree.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                scene.clear();
                scene.add(segment[degree.getValue()-1]);
                canvas.repaint();
            }
        });
        IntSlider degreeSlider = new IntSlider(degree, JSlider.HORIZONTAL, 1, 6);
        
        FloatParameter threshold = new FloatParameter("threshold", 0.5f);
        threshold.addChangeListener(repaintListener);
        for(int i = 0; i < segment.length; i++) {
            segment[i].threshold = threshold;
        }
        FloatSlider thresholdSlider = new FloatSlider(threshold, false, JSlider.HORIZONTAL, 0.5f, 50f);
        
        FloatParameter t = new FloatParameter("t", 0.3f);
        t.addChangeListener(repaintListener);
        for(int i = 0; i < segment.length; i++) {
            segment[i].deCasteljauT = t;
        }
        FloatSlider tSlider = new FloatSlider(t, false, JSlider.HORIZONTAL, 0f, 1f);
        
        IntParameter paintDeCasteljau = new IntParameter("deCasteljau", 0); 
        paintDeCasteljau.addChangeListener(repaintListener);
        for(int i = 0; i < segment.length; i++) {
            segment[i].paintDeCasteljau = paintDeCasteljau;
        }
        BooleanCheckBox paintDeCasteljauItem = new BooleanCheckBox("Show deCasteljau alg.", paintDeCasteljau);

        IntParameter divideCurve = new IntParameter("divideCurve", 0);
        divideCurve.addChangeListener(repaintListener);
        for(int i = 0; i < segment.length; i++) {
            segment[i].divideCurve = divideCurve;
        }
        BooleanCheckBox divideCurveItem = new BooleanCheckBox("Divide curve", divideCurve);

        IntParameter noOfIntervals = new IntParameter("noOfIntervals", 10);
        noOfIntervals.addChangeListener(repaintListener);
        for(int i = 0; i < segment.length; i++) {
            segment[i].noOfIntervals = noOfIntervals;
        }
        IntSlider noOfIntervalsSlider = new IntSlider(noOfIntervals, JSlider.HORIZONTAL, 1, 15);
        
        Dimension size = new Dimension(200, 800);
        JPanel panel = new JPanel();
        panel.setMaximumSize(size);
        panel.setMinimumSize(size);
        panel.setPreferredSize(size);
        panel.add(new ParameterLabel(threshold));
        panel.add(thresholdSlider);
        panel.add(new ParameterLabel(t));
        panel.add(tSlider);
        panel.add(new ParameterLabel(degree));
        panel.add(degreeSlider);
        panel.add(paintDeCasteljauItem);
        panel.add(divideCurveItem);
        panel.add(noOfIntervalsSlider);

        SpringLayout layout1 = new SpringLayout();
        this.setLayout(layout1);
        this.add(canvas);
        this.add(panel);
        layout1.putConstraint(SpringLayout.WEST, canvas, 0, SpringLayout.WEST, this);
        layout1.putConstraint(SpringLayout.NORTH, canvas, 0, SpringLayout.NORTH, this);
        layout1.putConstraint(SpringLayout.SOUTH, canvas, 0, SpringLayout.SOUTH, this);
        layout1.putConstraint(SpringLayout.EAST, panel, 0, SpringLayout.EAST, this);
        layout1.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.NORTH, this);
        layout1.putConstraint(SpringLayout.SOUTH, panel, 0, SpringLayout.SOUTH, this);
        layout1.putConstraint(SpringLayout.EAST, canvas, 0, SpringLayout.WEST, panel);
    }
}
