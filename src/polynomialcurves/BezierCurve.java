/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import javax.vecmath.Point3f;
import ui.parameter.FloatParameter;
import ui.parameter.IntParameter;

import javax.vecmath.Point3f;

/**
 *
 * @author cmolikl
 */
public class BezierCurve implements Curve {
    ControlPoint[][] cp;
    FloatParameter threshold = new FloatParameter("threshold", 0.1f);
    FloatParameter deCasteljauT = new FloatParameter("t", 0.5f);
    IntParameter paintDeCasteljau = new IntParameter("deCasteljau", 0); 
    IntParameter paintControls = new IntParameter("controlPoints", 1);
    IntParameter divideCurve = new IntParameter("divideCurve", 0);
    IntParameter noOfIntervals = new IntParameter("noFoIntervals", 10);

    public BezierCurve(ControlPoint... p) {
        cp = new ControlPoint[p.length][];
        cp[0] = p;
        for(int i = 1; i < cp.length; i++) {
            if(cp[i] == null) cp[i] = new ControlPoint[p.length-i];
            for(int j = 0; j < cp[i].length; j++) {
                cp[i][j] = new ControlPoint(new Point3f());
            }
        }
        
        cp[0][0].endPoint = true;
        cp[0][cp[0].length-1].endPoint = true;
        for(int i = 0; i < cp[0].length-1; i++) {
            cp[0][i].next = cp[0][i+1];
            cp[0][i+1].prev = cp[0][i];
        }
    }
    
    private void deCasteljau(float t) {
        for(int i = 1; i < cp.length; i++) {
            for(int j = 0; j < cp[i].length; j++) {
                cp[i][j].coords.interpolate(cp[i-1][j].coords, cp[i-1][j+1].coords, t);
            }
        }
    }
    
    public BezierCurve[] divide(float t) {
        deCasteljau(t);
        
        ControlPoint[] cp0 = new ControlPoint[cp.length];
        cp0[0] = new ControlPoint(cp[0][0]);
        for(int i = 1; i < cp.length; i++) {
            cp0[i] = cp[i][0];
        }
        
        ControlPoint[] cp1 = new ControlPoint[cp.length];
        cp1[cp[0].length-1] = new ControlPoint(cp[0][cp[0].length-1]);
        for(int i = 1; i < cp.length; i++) {
            int index = cp[i].length-1;
            cp1[index] = cp[i][index];
        }
        
        return new BezierCurve[] {new BezierCurve(cp0), new BezierCurve(cp1)};
    }
    
    private float getLinePointDistance(Point3f p1, Point3f p2, Point3f p) {
        float x = p2.x/p2.z - p1.x/p1.z;
        float y = p2.y/p2.z - p1.y/p2.z;
        float dist = (float) (Math.abs(x*(p1.y/p1.z - p.y/p.z) - (p1.x/p1.z - p.x/p.z)*y) / Math.sqrt(x*x + y*y));
        return dist;
    }
    
    private float getMaxDistance() {
        float maxDist = 0f;
        for(int i = 1; i < cp.length - 1; i++) {
            float dist = getLinePointDistance(cp[0][0].coords, cp[0][cp.length-1].coords, cp[0][i].coords);
            if(dist > maxDist) maxDist = dist;
        }
        return maxDist;
    }
    
    Line2D.Float line = new Line2D.Float();

    @Override
    public void paint(Graphics2D g) {
        if(threshold.getValue() >= getMaxDistance()) {
            line.x1 = cp[0][0].coords.x/cp[0][0].coords.z;
            line.y1 = cp[0][0].coords.y/cp[0][0].coords.z;
            line.x2 = cp[0][cp[0].length-1].coords.x/cp[0][cp[0].length-1].coords.z;
            line.y2 = cp[0][cp[0].length-1].coords.y/cp[0][cp[0].length-1].coords.z;
            g.draw(line);
        }
        else {
            BezierCurve[] halves = divide(0.5f);
            halves[0].threshold = threshold;
            halves[0].paint(g);
            halves[1].threshold = threshold;
            halves[1].paint(g);
        }
    }
    
    Ellipse2D.Float point = new Ellipse2D.Float();
    
    public void paintControls(Graphics2D g) {
        if(paintControls.getValue() != 0) {
            Color tmp = g.getColor();
            for(int i = 0; i < cp[0].length-1; i++) {
                if(cp[0][i].endPoint || cp[0][i+1].endPoint) {
                    g.setColor(Color.BLACK);
                }
                else {
                    g.setColor(Color.LIGHT_GRAY);
                }
                line.x1 = cp[0][i].coords.x/cp[0][i].coords.z;
                line.y1 = cp[0][i].coords.y/cp[0][i].coords.z;
                line.x2 = cp[0][i+1].coords.x/cp[0][i+1].coords.z;
                line.y2 = cp[0][i+1].coords.y/cp[0][i+1].coords.z;
                g.draw(line);
            }
            for(int i = 0; i < cp[0].length; i++) {
                if(cp[0][i].endPoint) {
                    g.setColor(Color.BLACK);
                }
                else {
                    g.setColor(Color.WHITE);
                }
                point.x = cp[0][i].coords.x - POINT_RADI;
                point.y = cp[0][i].coords.y - POINT_RADI;
                point.width = 2f*POINT_RADI;
                point.height = 2f*POINT_RADI;
                g.fill(point);
                if(!cp[0][i].endPoint) {
                    g.setColor(Color.BLACK);
                    g.draw(point);
                }
            }

            paintDivision(g);
            paintDeCasteljau(g);
            g.setColor(tmp);
        }
    }
    
    public static Color[] colors = new Color[] {Color.RED, Color.GREEN, Color.BLUE, Color.MAGENTA, Color.ORANGE, Color.CYAN};
    
    public void paintDeCasteljau(Graphics2D g) {
        deCasteljau(deCasteljauT.getValue());
        if(paintDeCasteljau.getValue() == 1) {
            Color tmp = g.getColor();
            for(int k = 1; k < cp.length; k++) {
                g.setColor(colors[k-1]);
                for(int i = 0; i < cp[k].length-1; i++) {
                    line.x1 = cp[k][i].coords.x/cp[0][i].coords.z;
                    line.y1 = cp[k][i].coords.y/cp[0][i].coords.z;
                    line.x2 = cp[k][i+1].coords.x/cp[0][i+1].coords.z;
                    line.y2 = cp[k][i+1].coords.y/cp[0][i+1].coords.z;
                    g.draw(line);
                }
                for(int i = 0; i < cp[k].length; i++) {
                    point.x = cp[k][i].coords.x - SMALL_RADI;
                    point.y = cp[k][i].coords.y - SMALL_RADI;
                    point.width = 2f*SMALL_RADI;
                    point.height = 2f*SMALL_RADI;
                    g.fill(point);
                }
            }
            g.setColor(tmp);
        }
        else if(paintDeCasteljau.getValue() == 0) {
            point.x = cp[cp.length-1][0].coords.x - SMALL_RADI;
            point.y = cp[cp.length-1][0].coords.y - SMALL_RADI;
            point.width = 2f*SMALL_RADI;
            point.height = 2f*SMALL_RADI;
            g.fill(point);
        }
    }

    public void paintDivision(Graphics2D g) {
        if(divideCurve.getValue() == 1) {
            float divisionInterval = 1f / (float) noOfIntervals.getValue();
            for(float t = 0f; t <= 1f; t += divisionInterval) {
                deCasteljau(t);
                point.x = cp[cp.length-1][0].coords.x - EXTRA_SMALL_RADI;
                point.y = cp[cp.length-1][0].coords.y - EXTRA_SMALL_RADI;
                point.width = 2f*EXTRA_SMALL_RADI;
                point.height = 2f*EXTRA_SMALL_RADI;
                g.fill(point);
            }
        }
    }

    @Override
    public ControlPoint select(Point2D cursor) {
        for(int i = 0; i < cp[0].length; i++) {
            point.x = cp[0][i].coords.x - POINT_RADI;
            point.y = cp[0][i].coords.y - POINT_RADI;
            point.width = 2f*POINT_RADI;
            point.height = 2f*POINT_RADI;
            if(point.contains(cursor)) {
                return cp[0][i];
            }
        }
        return null;
    }

    @Override
    public int getSelectedDegree() {
        return cp[0].length-1;
    }
}
