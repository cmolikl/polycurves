/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.BorderLayout;
import java.awt.Dimension;
import ui.control.Canvas;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.vecmath.Point3f;

import ui.control.OneOfN;
import ui.control.OneOfNMenuItem;
import ui.parameter.IntParameter;

/**
 *
 * @author cmolikl
 */
public class BezierPiecewiseExample extends JPanel {

    public BezierPiecewiseExample() {

        ControlPoint p0 = new ControlPoint(new Point3f(100f, 400f, 1f));
        ControlPoint p1 = new ControlPoint(new Point3f(200f, 100f, 1f));
        ControlPoint p2 = new ControlPoint(new Point3f(300f, 100f, 1f));
        ControlPoint p3 = new ControlPoint(new Point3f(400f, 400f, 1f));
        ControlPoint p4 = new ControlPoint(new Point3f(500f, 700f, 1f));
        ControlPoint p5 = new ControlPoint(new Point3f(600f, 700f, 1f));
        ControlPoint p6 = new ControlPoint(new Point3f(700f, 400f, 1f));
        BezierCurve segment0 = new BezierCurve(p0, p1, p2, p3);
        segment0.paintDeCasteljau.setValue(2);
        BezierCurve segment1 = new BezierCurve(p3, p4, p5, p6);
        segment1.paintDeCasteljau.setValue(2);
        BezierPiecewiseCurve curve = new BezierPiecewiseCurve();
        curve.addSegment(segment0);
        curve.addSegment(segment1);

        // Map data to graphical elements
        ArrayList<Curve> scene = new ArrayList<>();
        scene.add(curve);

        // Create instance of Canvas and provide the graphical elements
        Dimension cSize = new Dimension(1000, 800);
        final Canvas canvas = new Canvas(scene);
        //canvas.setMaximumSize(cSize);
        //canvas.setMinimumSize(cSize);
        //canvas.setPreferredSize(cSize);
        
        ChangeListener repaintListener = new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                canvas.repaint();
            }
        };
        
        ButtonGroup group = new ButtonGroup();
        
         ChangeListener continuityListener = new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                IntParameter ip = (IntParameter) e.getSource();
                if(ip.getValue() == 1) {
                    if("G1".equals(ip.name)) {
                        p3.continuity = ControlPoint.G1;
                        Point3f tmp = new Point3f(0f, 0f, 0f);
                        tmp.set(p3.coords);
                        tmp.sub(p2.coords);
                        
                        float dist23 = p3.coords.distance(p2.coords);
                        float dist34 = p3.coords.distance(p4.coords);
                        
                        float scale = dist34/dist23;
                        tmp.scale(scale);
                        
                        p4.coords.set(p3.coords);
                        p4.coords.add(tmp);
                        
                    }
                    else if("C1".equals(ip.name)) {
                        p3.continuity = ControlPoint.C1;
                        Point3f tmp = new Point3f(0f, 0f, 0f);
                        tmp.set(p3.coords);
                        tmp.sub(p2.coords);
                        
                        p4.coords.set(p3.coords);
                        p4.coords.add(tmp);
                    }
                    else if("C0".equals(ip.name)) {
                        p3.continuity = ControlPoint.C0;
                    }
                    canvas.repaint();
                }
            }
        };
        
        String[] names = new String[] {"C0", "G1", "C1"};
        int[] values = new int[] {1, 0, 0};
        
        
        
        Dimension size = new Dimension(200, 800);
        JPanel panel = new JPanel();
        SpringLayout layout = new SpringLayout();
        panel.setLayout(layout);
        panel.setMaximumSize(size);
        panel.setMinimumSize(size);
        panel.setPreferredSize(size);
        JLabel label = new JLabel("Continuity of segments");
        panel.add(label);
        layout.putConstraint(SpringLayout.WEST, label, 5, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, label, 5, SpringLayout.NORTH, panel);
        
        JComponent prevComponent = label;
        for (int i = 0; i < names.length; i++) {
            IntParameter ip = new IntParameter(names[i], values[i]);
            ip.addChangeListener(continuityListener);
            OneOfN menuItem = new OneOfN(names[i], group, ip);
            group.add(menuItem);
            panel.add(menuItem);
            layout.putConstraint(SpringLayout.WEST, menuItem, 5, SpringLayout.WEST, panel);
            layout.putConstraint(SpringLayout.NORTH, menuItem, 5, SpringLayout.SOUTH, prevComponent);
            prevComponent = menuItem;
        }

        // Put the canvas to JFrame
        SpringLayout layout1 = new SpringLayout();
        this.setLayout(layout1);
        this.add(canvas);
        this.add(panel);
        layout1.putConstraint(SpringLayout.WEST, canvas, 0, SpringLayout.WEST, this);
        layout1.putConstraint(SpringLayout.NORTH, canvas, 0, SpringLayout.NORTH, this);
        layout1.putConstraint(SpringLayout.SOUTH, canvas, 0, SpringLayout.SOUTH, this);
        layout1.putConstraint(SpringLayout.EAST, panel, 0, SpringLayout.EAST, this);
        layout1.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.NORTH, this);
        layout1.putConstraint(SpringLayout.SOUTH, panel, 0, SpringLayout.SOUTH, this);
        layout1.putConstraint(SpringLayout.EAST, canvas, 0, SpringLayout.WEST, panel);

    }
}
