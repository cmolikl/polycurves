/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JPanel;
import ui.control.Canvas;

/**
 *
 * @author cmolikl
 */
public class BSplineParametrization extends JPanel {
    
    private static int THRESHOLD = 5; 
    private static int MIN_KNOT_DISTANCE = 2;
    private boolean drag = false;
    private int startX;
    private float startValue;
    private int knotIndex;
    
    private class ParametrizationMouseListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent me) {
            int x = me.getX();
            float parametrizationLength = curve.knots[curve.knots.length-1] - curve.knots[0];
            float knotScale = canvasWidth/parametrizationLength;
            float minKnotDistance = Float.MAX_VALUE;
            for(int i = 0; i < curve.knots.length; i++) {
                float knotDistance = Math.abs(x - (curve.knots[i]-curve.knots[0])*knotScale);
                if(knotDistance < THRESHOLD) {
                    if(knotDistance < minKnotDistance && i > 0 && i < curve.knots.length-1) {
                        startX = x;
                        startValue = curve.knots[i];
                        knotIndex = i;
                        minKnotDistance = knotDistance;
                        drag = true;
                    }
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent me) {
            drag = false;
            int x = me.getX();
            float parametrizationLength = curve.knots[curve.knots.length-1] - curve.knots[0];
            float knotScale = canvasWidth/parametrizationLength;
            float minKnotDistance = Float.MAX_VALUE;
            int currentKnotIndex = -1;
            for(int i = 0; i < curve.knots.length; i++) {
                float knotDistance = Math.abs(x - (curve.knots[i]-curve.knots[0])*knotScale);
                if(knotDistance < THRESHOLD) {
                    if(knotDistance < minKnotDistance && i > 0 && i < curve.knots.length-1) {
                        startX = x;
                        startValue = curve.knots[i];
                        currentKnotIndex = i;
                        minKnotDistance = knotDistance;
                    }
                }
            }
            if(currentKnotIndex != knotIndex) {
                knotIndex = currentKnotIndex;
                repaint();
            }
        }        
    } 
    
    private class ParametrizationMouseMotionListener implements MouseMotionListener {
        @Override
        public void mouseDragged(MouseEvent me) {
            if(drag) {
                int endX = me.getX();
                float parametrizationLength = curve.knots[curve.knots.length-1] - curve.knots[0];
                float canvasScale = parametrizationLength/canvasWidth;
                float knotMovement = (endX - startX)*canvasScale;
                float minKnotDistance = MIN_KNOT_DISTANCE*canvasScale;
                if(knotMovement < 0) {
                    if(curve.knots[knotIndex-1] + minKnotDistance > startValue + knotMovement) {
                        curve.knots[knotIndex] = curve.knots[knotIndex-1] + minKnotDistance;
                    }
                    else {
                        curve.knots[knotIndex] = startValue + knotMovement;
                    }
                    repaint();
                    canvas.repaint();
                }
                else {
                    if(curve.knots[knotIndex+1] - minKnotDistance < startValue + knotMovement) {
                        curve.knots[knotIndex] = curve.knots[knotIndex+1] - minKnotDistance;
                    }
                    else {
                        curve.knots[knotIndex] = startValue + knotMovement;
                    }
                    repaint();
                    canvas.repaint();
                }
            }
        }

        @Override
        public void mouseMoved(MouseEvent me) {
            if(!drag) {
                int x = me.getX();
                float parametrizationLength = curve.knots[curve.knots.length-1] - curve.knots[0];
                float knotScale = canvasWidth/parametrizationLength;
                float minKnotDistance = Float.MAX_VALUE;
                int currentKnotIndex = -1;
                for(int i = 0; i < curve.knots.length; i++) {
                    float knotDistance = Math.abs(x - (curve.knots[i]-curve.knots[0])*knotScale);
                    if(knotDistance < THRESHOLD) {
                        if(knotDistance < minKnotDistance && i > 0 && i < curve.knots.length-1) {
                            startX = x;
                            startValue = curve.knots[i];
                            currentKnotIndex = i;
                            minKnotDistance = knotDistance;
                        }
                    }
                }
                if(currentKnotIndex != knotIndex) {
                    knotIndex = currentKnotIndex;
                    repaint();
                }
            }
        }
    }
    
    private BSplineCurve curve;
    private Canvas canvas;
    private float canvasWidth = 1000f;
    private float canvasHeight = 200f;
    
    public BSplineParametrization(BSplineCurve curve, Canvas canvas) {
        this.curve = curve;
        this.canvas = canvas;
        this.addMouseListener(new ParametrizationMouseListener());
        this.addMouseMotionListener(new ParametrizationMouseMotionListener());
    }
    
    Line2D.Float line = new Line2D.Float();
    Rectangle2D.Float rect = new Rectangle2D.Float();

    @Override
    public void paint(Graphics g) {
        canvasWidth = getWidth();
        canvasHeight = getHeight();
        Graphics2D g2d = (Graphics2D) g;
        float[] knots = curve.knots;
        int order = curve.order;
        
        float parametrizationLength = knots[knots.length-1] - knots[0];
        float knotsScale = canvasWidth/parametrizationLength;
        float minX = (knots[order - 1] - knots[0])*knotsScale;
        float maxX = (knots[knots.length - order] - knots[0])*knotsScale;
        rect.x = 0f;
        rect.y = 0f;
        rect.width = canvasWidth;
        rect.height = canvasHeight;
        g2d.setColor(Color.WHITE);
        g2d.fill(rect);
        
        g2d.setColor(Color.LIGHT_GRAY);
        minX = 0f;
        maxX = (knots[1]-knots[0])*knotsScale;
        rect.x = minX;
        rect.y = 0f;
        rect.width = maxX - minX;
        rect.height = canvasHeight;
        g2d.fill(rect);
        minX = (knots[knots.length-2]-knots[0])*knotsScale;
        maxX = (knots[knots.length-1]-knots[0])*knotsScale;
        rect.x = minX;
        rect.y = 0f;
        rect.width = maxX - minX;
        rect.height = canvasHeight;
        g2d.fill(rect);
        
        //g2d.setColor(Color.GRAY);
        for(int i = 0; i < knots.length; i++) {
            if(i == knotIndex) {
                g2d.setColor(Color.RED);
            }
            else {
                g2d.setColor(Color.GRAY);
            }
            line.x1 = (knots[i]-knots[0])*knotsScale;
            line.y1 = 0f;
            line.x2 = (knots[i]-knots[0])*knotsScale;
            line.y2 = canvasHeight;
            g2d.draw(line);
        }
        float offset = parametrizationLength/canvasWidth;
        Float prevU = null;
        Float prevWeight = null;
        g2d.setColor(Color.BLACK);
        for(int i = 0; i < knots.length-order; i++) {
            for(float u = knots[0]; u < knots[knots.length-1]; u += offset) {
                float weight = getWeight(i, u);
                if(prevWeight != null) {
                    line.x1 = (prevU-knots[0])*knotsScale;
                    line.y1 = (1f-prevWeight)*canvasHeight;
                    line.x2 = (u-knots[0])*knotsScale;
                    line.y2 = (1f-weight)*canvasHeight;
                    g2d.draw(line);
                }
                prevU = u;
                prevWeight = weight;
            }
        }
    }
    
    public float getWeight(int i, float u) {
        if(u < curve.knots[0]) {
            u = curve.knots[0];
        }
        else if(u > curve.knots[curve.knots.length-1]) {
            u = curve.knots[curve.knots.length-1];
        }
        return basis(i, curve.order, u);
    }
    
    private float basis(int i, int k, float u) {
        if(k == 1) {
            if(curve.knots[i] <= u && u < curve.knots[i+1]) {
                return 1f;
            }
            else {
                return 0f;
            }
        }
        else {
            return (u - curve.knots[i])/(curve.knots[i+k-1] - curve.knots[i])*basis(i, k-1, u) + 
                   (curve.knots[i+k] - u)/(curve.knots[i+k] - curve.knots[i+1])*basis(i+1, k-1, u);
        }
    } 
}
