/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import javax.vecmath.Point3f;
import ui.parameter.FloatParameter;
import ui.parameter.IntParameter;

/**
 *
 * @author cmolikl
 */
public class BSplineCurve implements Curve {
    private ControlPoint[] cp;
    float[] knots;
    int order;
    
    private BezierPiecewiseCurve bezierCurve;
    private boolean change = true;
    
    public IntParameter paintDeBoor = new IntParameter("Show deBoor", 0);
    public IntParameter displaySegments = new IntParameter("Display segments", 0);
    
    public BSplineCurve(ControlPoint[] cp, int order) {
        this.cp = cp;
        this.order = order;
        
        this.knots = new float[cp.length + order];
        for(int i = 0; i < cp.length + order; i++) {
            knots[i] = i+1;
        }
        
        for(int i = 0; i < cp.length-1; i++) {
            cp[i].next = cp[i+1];
            cp[i+1].prev = cp[i];
        }
        
        initDeBoorPoints();
        
    }
    
    public void setOrder(int order) {
        this.order = order;
        initDeBoorPoints();
        
        float[] oldKnots = this.knots;
        this.knots = new float[cp.length + order];
        int knotsLength = Math.min(oldKnots.length, knots.length);
        for(int i = 0; i < knotsLength; i++) {
            this.knots[i] = oldKnots[i];
        }
        if(oldKnots.length < this.knots.length) {
            for(int i = knotsLength; i < this.knots.length; i++) {
                this.knots[i] = i+1;
            }
        }
    }
    
    private void initDeBoorPoints() {
        this.deBoorPoints = new Point3f[order][];
        for(int i = 0; i < deBoorPoints.length; i++) {
            if(deBoorPoints[i] == null) deBoorPoints[i] = new Point3f[deBoorPoints.length-i];
            for(int j = 0; j < deBoorPoints[i].length; j++) {
                deBoorPoints[i][j] = new Point3f();
            }
        }
    }
    
//    public BezierPiecewiseCurve toQuadraticBezier() {
//        BezierPiecewiseCurve bc = new BezierPiecewiseCurve();
//        
//        for(int i = 0; i < cp.length-2; i++) {
//            Point3f d0 = cp[i];
//            Point3f d1 = cp[i+1];
//            Point3f d2 = cp[i+2];
//            
//            float segment0 = knots[i+3] - knots[i+1];
//            float t0 = (knots[i+2] - knots[i+1])/segment0;
//            
//            float segment1 = knots[i+4] - knots[i+2];
//            float t1 = (knots[i+3] - knots[i+2])/segment1;
//            
//            Point3f b0 = new Point3f();
//            b0.interpolate(d0, d1, t0);
//            Point3f b1 = d1;
//            Point3f b2 = new Point3f();
//            b2.interpolate(d1, d2, t1);
//            
//            BezierCurve bs = new BezierCurve(b0, b1, b2);
//            bc.addSegment(bs);
//            
//            b0 = b2;
//        }
//        return bc;
//    }
    
    // Works for curves from order 2 to order 4
    public BezierPiecewiseCurve toBezier() {
        BezierPiecewiseCurve bc = new BezierPiecewiseCurve();
        
        boolean first = true;
        ControlPoint[] b = null;
        int div = order/2;
        int sub = order%2;
        int k = 0;
        for(int i = order-1; i < knots.length-order; i++) {
            
            deBoor(knots[i]);
            
            if(!first) {
                k = order-1;
                int j = order-2;
                for(int n = 0; n < div; n++) {
                    b[k] = new ControlPoint(new Point3f(deBoorPoints[j][0]));     
                    k--;  
                    j--;
                }
                //Create BezierSegment
                BezierCurve segment = new BezierCurve(b);
                bc.addSegment(segment);
            }
            b = new ControlPoint[order];
            k = 0;
            int j = order-1;
            for(int n = 0; n < div+sub; n++) {
                b[k] = new ControlPoint(new Point3f(deBoorPoints[j][deBoorPoints[j].length - 1]));     
                k++;     
                j--;
            }
            
            first = false;
        }
        
        deBoor(knots[knots.length-order]);
        
        k = order-1;
        int j = order-1;
        for(int n = 0; n < div; n++) {
            b[k] = new ControlPoint(new Point3f(deBoorPoints[j][0]));     
            k--;  
            j--;
        }
        //Create BezierSegment
        BezierCurve segment = new BezierCurve(b);
        bc.addSegment(segment);
        
        return bc;
    }
    
//    public BezierPiecewiseCurve toCubicBezier() {
//        BezierPiecewiseCurve bc = new BezierPiecewiseCurve();
//        Point3f b0 = null;
//        Point3f b1 = null;
//        
//        for(int i = 0; i < cp.length-2; i++) {
//            Point3f d0 = cp[i];
//            Point3f d1 = cp[i+1];
//            Point3f d2 = cp[i+2];
//            
//            float segment0 = knots[i+4] - knots[i+1];
//            float t0 = (knots[i+3] - knots[i+1])/segment0;
//            
//            float segment1 = knots[i+5] - knots[i+2];
//            float t1 = (knots[i+3] - knots[i+2])/segment1;
//            
//            float t2 = (knots[i+3] - knots[i+2])/(knots[i+4] - knots[i+2]);
//            
//            Point3f b2 = new Point3f();
//            b2.interpolate(d0, d1, t0);
//            Point3f h = new Point3f();
//            h.interpolate(d1, d2, t1);
//            
//            Point3f b3 = new Point3f();
//            b3.interpolate(b2, h, t2);
//            
//            if(b0 != null) {
//                BezierCurve bs = new BezierCurve(b0, b1, b2, b3);
//                bc.addSegment(bs);
//            }
//            
//            b0 = b3;
//            b1 = h;
//        }
//        return bc;
//    }

    @Override
    public void paint(Graphics2D g) {
        if(bezierCurve == null || change) {
            //toBezier();
            //if(order == 3) {
                bezierCurve = toBezier();
                //bezierCurve = toQuadraticBezier();
//            }
//            else if(order == 4) {
//                bezierCurve = toBezier();
//                //bezierCurve = toCubicBezier();
//            }
        }
        if(bezierCurve != null) {
            Graphics2D g2d = (Graphics2D) g;
            for(BezierCurve segment : bezierCurve.segments) {
                if (displaySegments.getValue() == 0) {
                    segment.divideCurve.setValue(0);
                } else {
                    segment.divideCurve.setValue(1);
                    segment.noOfIntervals.setValue(1);
                    segment.paintDivision(g2d);
                }
            }


            if (displaySegments.getValue() == 0) {
                bezierCurve.colorSegments.setValue(0);
            } else {
                bezierCurve.colorSegments.setValue(1);
            }
            bezierCurve.paint(g2d);
        }
    }
    
    Line2D.Float line = new Line2D.Float();
    Ellipse2D.Float point = new Ellipse2D.Float();
    
    @Override
    public void paintControls(Graphics2D g) {
        for(int i = 0; i < cp.length-1; i++) {
            line.x1 = cp[i].coords.x/cp[i].coords.z;
            line.y1 = cp[i].coords.y/cp[i].coords.z;
            line.x2 = cp[i+1].coords.x/cp[i+1].coords.z;
            line.y2 = cp[i+1].coords.y/cp[i+1].coords.z;
            g.draw(line);
        }
        for(int i = 0; i < cp.length; i++) {
            point.x = cp[i].coords.x/cp[i].coords.z - POINT_RADI;
            point.y = cp[i].coords.y/cp[i].coords.z - POINT_RADI;
            point.width = 2f*POINT_RADI;
            point.height = 2f*POINT_RADI;
            g.fill(point);
        }
        
        paintDeBoor(g);
    }
    
    public static Color[] colors = new Color[] {Color.RED, Color.GREEN, Color.BLUE, Color.MAGENTA, Color.ORANGE, Color.CYAN};
    
    public void paintDeBoor(Graphics2D g) {
        deBoorNormalized(deBoorT.getValue());
        if(paintDeBoor.getValue() == 1) {
            Color tmp = g.getColor();
            for(int k = 1; k < order; k++) {
                g.setColor(colors[k-1]);
                for(int i = 0; i < deBoorPoints[k].length - 1; i++) {
                    line.x1 = deBoorPoints[k][i].x/deBoorPoints[k][i].z;
                    line.y1 = deBoorPoints[k][i].y/deBoorPoints[k][i].z;
                    line.x2 = deBoorPoints[k][i+1].x/deBoorPoints[k][i+1].z;
                    line.y2 = deBoorPoints[k][i+1].y/deBoorPoints[k][i+1].z;
                    g.draw(line);
                }
                for(int i = 0; i < deBoorPoints[k].length; i++) {
                    point.x = deBoorPoints[k][i].x - SMALL_RADI;
                    point.y = deBoorPoints[k][i].y - SMALL_RADI;
                    point.width = 2f*SMALL_RADI;
                    point.height = 2f*SMALL_RADI;
                    g.fill(point);
                }
            }
            g.setColor(tmp);
        }
        else {
            point.x = deBoorPoints[order-1][0].x - SMALL_RADI;
            point.y = deBoorPoints[order-1][0].y - SMALL_RADI;
            point.width = 2f*SMALL_RADI;
            point.height = 2f*SMALL_RADI;
            g.fill(point);
        }
    }
    
    @Override
    public ControlPoint select(Point2D cursor) {
        for(int i = 0; i < cp.length; i++) {
            point.x = cp[i].coords.x - POINT_RADI;
            point.y = cp[i].coords.y - POINT_RADI;
            point.width = 2f*POINT_RADI;
            point.height = 2f*POINT_RADI;
            if(point.contains(cursor)) {
                return cp[i];
            }
        }
        return null;
    }

    @Override
    public int getSelectedDegree() {
        return order-1;
    }

    Point3f[][] deBoorPoints;
    FloatParameter deBoorT = new FloatParameter("t", 0.5f);
    
    public void deBoorNormalized(float t) {
        float u = knots[order-1]*(1-t) + knots[knots.length-order]*t;
        deBoor(u);
    }
    
    public void deBoor(float u) {    
        float delta = 0.000001f;
        if(u == knots[knots.length-order]) {
            u -= delta;
        }
        
        //Find knot lower and greater than deBoorT
        int I = -1;
        for(int i = 0; i < knots.length-1; i++) {
            if(knots[i] <= u && u < knots[i+1]) {
                I = i;
                break;
            }
        }
        
        int degree = order - 1;
        
        //Initialize the lowest level of point pyramid with control points
        int index = 0;
        for(int i = I - degree + 1; i < I + 2; i++) {
            deBoorPoints[0][index] = cp[i-1].coords;
            index++;
        }
        
        //Go through the pyramid of points
        for(int k = 1; k < order; k++) {
            index = 0;
            //At each level calculate deBoor points
            for(int i = I - degree + k + 1; i < I + 2; i++) {
                //System.out.println("t = " + t);
                //System.out.println("Knot at " + (i-1) + " = " + knots[i-1]);
                //System.out.println("Knot at " + (i+degree-k) + " = " + knots[i+degree-k]);
                //System.out.println("Interpolating with " + (t - knots[i-1])/(knots[i+degree-k] - knots[i-1]));
                deBoorPoints[k][index].interpolate(deBoorPoints[k-1][index], deBoorPoints[k-1][index+1], (u - knots[i-1])/(knots[i+degree-k] - knots[i-1]));
                index++;
            }
            
        }
        
    }
    
}
