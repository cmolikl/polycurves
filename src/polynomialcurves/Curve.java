/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import javax.vecmath.Point3f;

/**
 *
 * @author cmolikl
 */
public interface Curve {
    public static final float POINT_RADI = 10f;
    public static final float SMALL_RADI = 6f;
    public static final float EXTRA_SMALL_RADI = 3f;
    
    public void paint(Graphics2D g);
    public void paintControls(Graphics2D g);
    //public Rectangle2D getBounds();
    public ControlPoint select(Point2D cursor);
    public int getSelectedDegree();
}
