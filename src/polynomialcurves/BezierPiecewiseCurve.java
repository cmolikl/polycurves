/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import ui.parameter.IntParameter;

import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import javax.vecmath.Point3f;

/**
 *
 * @author cmolikl
 */
public class BezierPiecewiseCurve implements Curve {
    ArrayList<BezierCurve> segments = new ArrayList<>();

    IntParameter colorSegments = new IntParameter("colorSegments", 0);
    
    public BezierCurve getSegment(int i) {
        return segments.get(i);
    }
    
    public void addSegment(BezierCurve segment) {
//        BezierCurve lastSegment = null;
//        ControlPoint lastCp = null;
//        ControlPoint firstCp = segment.cp[0][0];
//        if(segments.size() > 0) {
//            lastSegment = segments.get(segments.size()-1);
//            lastCp = lastSegment.cp[0][lastSegment.cp[0].length-1];
//            if(lastCp == firstCp) {
//                lastCp.next = firstCp;
//                firstCp.prev = lastCp;
//            }
//        }
        segments.add(segment);
        
    }
    
    @Override
    public void paint(Graphics2D g) {
        int i = 0;
        for(BezierCurve s : segments) {
            if(colorSegments.getValue() == 0) {
                s.paint(g);
            }
            else {
                g.setColor(BezierCurve.colors[i % BezierCurve.colors.length]);
                s.paint(g);
                g.setColor(Color.BLACK);
            }
            i++;
        }
    }
    
    @Override
    public void paintControls(Graphics2D g) {
        for(BezierCurve s : segments) {
            s.paintControls(g);
        }
    }

    @Override
    public ControlPoint select(Point2D cursor) {
        for(BezierCurve s : segments) {
            ControlPoint selectedCp = s.select(cursor);
            if(selectedCp != null) {
                selectedDegree = s.getSelectedDegree();
                return selectedCp;
            }
        }
        return null;
    }

    int selectedDegree = 0;
    @Override
    public int getSelectedDegree() {
        return selectedDegree;
    }
}
