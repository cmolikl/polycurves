/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynomialcurves;

import java.awt.Color;
import javax.vecmath.Point3f;

/**
 *
 * @author cmolikl
 */
public class ControlPoint {
    
    public static final float POINT_RADI = 10f;
    public static final float SMALL_RADI = 6f;
    
    public static int C0, G0 = 0;
    public static int C1 = 1;
    public static int C2 = 2;
    public static int G1 = 3;

    public ControlPoint prev;
    public ControlPoint next;
    public Point3f coords;
    public Color color = Color.BLACK;
    public float radius;
    public int continuity = C0;
    public boolean endPoint = false;
    
    public ControlPoint(ControlPoint cp) {
        this.prev = cp.prev;
        this.next = cp.next;
        this.coords = cp.coords;
        this.color = cp.color;
        this.radius = cp.radius;
        this.continuity = cp.continuity;
        this.endPoint = cp.endPoint;
    }
    
    public ControlPoint(Point3f coords) {
        this.coords = coords;
        this.radius = POINT_RADI;
    }
    
    public ControlPoint(Point3f coords, Color color) {
        this(coords);
        this.color = color;
    }
}
