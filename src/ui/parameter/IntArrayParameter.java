/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.parameter;

import java.util.StringTokenizer;

/**
 *
 * @author cmolikl
 */
public class IntArrayParameter extends Parameter {

    int[] value;
    private int length;
    private int offset = 0;

    public IntArrayParameter(String name, int[] value) {
        super(name);
        this.value = value;
        length = value.length;
    }

//    public GlslProgramIntArrayParameter(String name, int[] value, int offset, int length) {
//        super(name);
//        this.value = value;
//        this.offset = offset;
//        this.length = length;
//    }
    
    public void setValue(int[] value) {
        this.value = value;
        length = value.length;
    }
    
//    public void setValue(int[] value, int offset, int length) {
//        this.value = value;
//        this.offset = offset;
//        this.length = length;
//    }
    
    public boolean parseValue(String string) {
       try {
           StringTokenizer tokenizer = new StringTokenizer(string, " ");
           length = tokenizer.countTokens();
           if(length == 0) {
               return false;
           } 
           if(value.length != length) {
               value = new int[length];
           }
           for(int i = 0; i < length; i++) {
                value[i] = Integer.parseInt(tokenizer.nextToken());
           }
       }
       catch(Exception e) {
           return false;
       }
       return true;
    }
    
    public String toString() {
        StringBuilder buffer = new StringBuilder();
        for(int i = 0; i < length; i++) {
            buffer.append(value[i]);
            if(i + 1 == length) break;
            buffer.append(" ");
        }
        return buffer.toString();
    }
}
