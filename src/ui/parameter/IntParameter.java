/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.parameter;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author cmolikl
 */
public class IntParameter extends Parameter {
    int value;
    
    public IntParameter(String name, int value) {
        super(name);
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
    public void setValue(int value) {
        if(this.value != value) {
            this.value = value;
            ChangeEvent e = new ChangeEvent(this);
            for(ChangeListener l : listeners) {
                l.stateChanged(e);
            }
        }
    }
    
    public boolean parseValue(String string) {
       try {
           setValue(Integer.parseInt(string));
       }
       catch(Exception e) {
           return false;
       }
       return true;
    }
    
    public String toString() {
        return "" + value;
    }
}
