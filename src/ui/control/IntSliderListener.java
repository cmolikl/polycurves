/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ui.control;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import ui.parameter.IntParameter;

/**
 *
 * @author cmolikl
 */
public class IntSliderListener implements ChangeListener {
     IntParameter param;

     public IntSliderListener(IntParameter param) {
         this.param = param;
     }

     public void stateChanged(ChangeEvent e) {
        IntSlider slider = (IntSlider) e.getSource();
        param.setValue(slider.getValue());
    }
}
