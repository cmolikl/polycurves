/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.control;

import ui.parameter.IntParameter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

/**
 *
 * @author Jakub
 */
public class OneOfNMenuItemActionListener implements ActionListener {

    IntParameter param;
    ButtonGroup group;

    public OneOfNMenuItemActionListener(IntParameter param, ButtonGroup group) {
        this.param = param;
        this.group = group;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Enumeration<AbstractButton> enumeration = group.getElements();
        for (int i = 0; i < group.getButtonCount(); i++) {
            OneOfNMenuItem button = (OneOfNMenuItem) enumeration.nextElement();
            button.getParam().setValue(0);
        }
        param.setValue(1);
    }

}
