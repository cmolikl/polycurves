/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui.control;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.ListIterator;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicSliderUI;
import javax.vecmath.Point3f;
import polynomialcurves.ControlPoint;
import polynomialcurves.Curve;

/**
 *
 * @author cmolikl
 */
public class Canvas extends JPanel {
    
    private class ZoomHandler implements MouseWheelListener {
        
        private static final float ZOOM_IN_FACTOR = 1.05f;
        private static final float ZOOM_OUT_FACTOR = 1f/ZOOM_IN_FACTOR;
        private final AffineTransform zoomTransform = new AffineTransform();
        
        @Override
        public void mouseWheelMoved(MouseWheelEvent event) {
            float zoom = 1f;
            if (event.getPreciseWheelRotation() > 0.0) {
                zoom = ZOOM_OUT_FACTOR;
            } else {
                zoom = ZOOM_IN_FACTOR;
            }

            zoomLevel *= zoom;
            
            float x = dpiScale*event.getX();
            float y = dpiScale*event.getY();
            
            zoomTransform.setToTranslation(x, y);
            zoomTransform.scale(zoom, zoom);
            zoomTransform.translate(-x, -y);
            
            transform.preConcatenate(zoomTransform);
            
            SwingUtilities.invokeLater(() -> repaint());
        }
    }
    
    private class PanHandler extends MouseAdapter implements MouseMotionListener {
        
        private float startX;
        private float startY;
        private final AffineTransform translate = new AffineTransform();

        @Override
        public void mouseDragged(MouseEvent event) {
            float endX = dpiScale*event.getXOnScreen();
            float endY = dpiScale*event.getYOnScreen();
            float translateX = endX - startX;
            float translateY = endY - startY;
            if(selectedCp != null) {
                float tx = translateX/(zoomLevel*dpiScale);
                float ty = translateY/(zoomLevel*dpiScale);
                selectedCp.coords.x += tx;
                selectedCp.coords.y += ty;
                if(selectedCp.endPoint && selectedDegree > 2) {
                    if(selectedCp.prev != null) {
                        selectedCp.prev.coords.x += tx;
                        selectedCp.prev.coords.y += ty;
                    }
                    if(selectedCp.next != null) {
                        selectedCp.next.coords.x += tx;
                        selectedCp.next.coords.y += ty;
                    }
                }
                else {
                    ControlPoint endPoint = null;
                    ControlPoint sibling = null;
                    if(selectedCp.next != null && selectedCp.next.endPoint) {
                        endPoint = selectedCp.next;
                        sibling = endPoint.next;
                    }
                    else if(selectedCp.prev != null && selectedCp.prev.endPoint) {
                        endPoint = selectedCp.prev;
                        sibling = endPoint.prev;
                    }
                    if(sibling != null && endPoint.continuity != ControlPoint.C0) {
                        if(endPoint.continuity == ControlPoint.C1) {
                            Point3f tmp = new Point3f(0f, 0f, 0f);
                            tmp.set(endPoint.coords);
                            tmp.sub(selectedCp.coords);

                            sibling.coords.set(endPoint.coords);
                            sibling.coords.add(tmp);
                        }
                        else if(endPoint.continuity == ControlPoint.G1) {
                            Point3f tmp = new Point3f(0f, 0f, 0f);
                            tmp.set(endPoint.coords);
                            tmp.sub(selectedCp.coords);

                            float dist23 = endPoint.coords.distance(selectedCp.coords);
                            float dist34 = endPoint.coords.distance(sibling.coords);

                            float scale = dist34/dist23;
                            tmp.scale(scale);

                            sibling.coords.set(endPoint.coords);
                            sibling.coords.add(tmp);
                        }
                    }
                }
            }
            else {
                translate.setToTranslation(translateX, translateY);
                transform.preConcatenate(translate);
            }
            startX = endX;
            startY = endY;
            SwingUtilities.invokeLater(() -> repaint());
        }

        @Override
        public void mouseMoved(MouseEvent event) {
        }

        @Override
        public void mousePressed(MouseEvent event) {
            startX = dpiScale*event.getXOnScreen();
            startY = dpiScale*event.getYOnScreen();
            AffineTransform invTransform = null;
            try {
                invTransform = transform.createInverse();
            }
            catch(NoninvertibleTransformException e) {
                e.printStackTrace();
            }
            if(invTransform != null) {
                Point2D p = (Point2D) event.getPoint();
                p.setLocation(dpiScale*p.getX(), dpiScale*p.getY());
                Point2D tp = invTransform.transform(p, null);
                tp.setLocation(tp.getX()/dpiScale, tp.getY()/dpiScale);
                
                ListIterator<Curve> li = scene.listIterator(scene.size());
                while(li.hasPrevious()) {
                    Curve ge = li.previous();
                    selectedCp = ge.select(tp);
                    if(selectedCp != null) {
                        selectedDegree = ge.getSelectedDegree();
                        break;
                    }
                }
            }
        }
        
        @Override
        public void mouseReleased(MouseEvent event) {
            selectedCp = null;
        }
    }
    
//    private class SelectionHandler extends MouseAdapter {
//        @Override
//        public void mouseClicked(MouseEvent event) {
//            AffineTransform invTransform = null;
//            try {
//                invTransform = transform.createInverse();
//            }
//            catch(NoninvertibleTransformException e) {
//                e.printStackTrace();
//            }
//            if(invTransform != null) {
//                Point2D p = (Point2D) event.getPoint();
//                p.setLocation(dpiScale*p.getX(), dpiScale*p.getY());
//                Point2D tp = invTransform.transform(p, null);
//                
//                ListIterator<Drawable> li = scene.listIterator(scene.size());
//                while(li.hasPrevious()) {
//                    Drawable ge = li.previous();
//                    selectedCp = ge.select(tp);
//                    if(selectedCp != null) {
//                        break;
//                    }
//                }
//                repaint();
//            }
//        }
//    }
    
    private static final BasicStroke stroke = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    ArrayList<Curve> scene;
    Rectangle2D sceneBounds;
    AffineTransform transform;
    AffineTransform seeAllTransform;
    Dimension dim;
    float dpiScale = 1.0f;
    ControlPoint selectedCp = null;
    int selectedDegree = 0;
    float zoomLevel = 1f;
    
    public Canvas(ArrayList<Curve> graphicalElements) {
        //Try to get DPI scaling
        int dpi = Toolkit.getDefaultToolkit().getScreenResolution();
        String version = System.getProperty("java.version");
        if(!version.startsWith("1.")) {
            dpiScale = dpi/96.0f;
        }
        // Store the scene
        this.scene = graphicalElements;

        transform = new AffineTransform();
        transform.setToIdentity();
        
        //Setup event listeners
        addMouseWheelListener(new ZoomHandler());
        PanHandler panHandler = new PanHandler();
        addMouseListener(panHandler);
        addMouseMotionListener(panHandler);
        //addMouseListener(new SelectionHandler());
    }
    
//    private void calculateSeeAllTransform() {
//        // Calculate inital transformation matrix to see the whole scene
//        double translateX = 0.0;
//        if(sceneBounds.getX() < 0.0) {
//            translateX = -sceneBounds.getX();
//        }
//        double translateY = 0.0;
//        if(sceneBounds.getY() < 0.0) {
//            translateY = -sceneBounds.getY();
//        }
//        
//        double scale = Math.min(dpiScale*dim.width/(sceneBounds.getWidth() + 10), dpiScale*dim.height/(sceneBounds.getHeight() + 10));
//        
//        seeAllTransform = new AffineTransform();
//        seeAllTransform.scale(scale, scale);
//        seeAllTransform.translate(translateX + 5, translateY + 5);
//    }
    
    @Override 
    public void paintComponent(Graphics g) {
        //super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setBackground(Color.WHITE);
        g2d.clearRect(0, 0, getWidth(), getHeight());
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setStroke(stroke);
        // Store transformation matrix
        AffineTransform imageTransform = g2d.getTransform();
        AffineTransform tmp = (AffineTransform) imageTransform.clone();
        tmp.preConcatenate(transform);
        // Set our transformation matrix
        g2d.setTransform(tmp);
        // Draw the scene
        for(Curve ge : scene) {
            ge.paint(g2d);
            ge.paintControls(g2d);
        }
        g2d.setTransform(imageTransform);
        
    }
    
//    @Override
//    public void setBounds(Rectangle r) {
//        super.setBounds(r);
//        Dimension dim = new Dimension(r.width, r.height);
//        this.setPreferredSize(dim);
//    }
}
